package cat.itb.diceroller;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button rollButton;
    ImageView imageView1;
    ImageView imageView2;
    Button resetButton;

    private int [] ArrayDraw = {
        R.drawable.dice_1,
        R.drawable.dice_2,
        R.drawable.dice_3,
        R.drawable.dice_4,
        R.drawable.dice_5,
        R.drawable.dice_6,
    };

    public int i = 0;
    public int j = 0;

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rollButton = findViewById(R.id.roll_button);
        imageView1 = findViewById(R.id.dice_1);
        imageView2 = findViewById(R.id.dice_2);
        resetButton = findViewById(R.id.roll_button2);

        rollButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rollButton.setText("Dice Rolled");
                i = (int) (Math.random()*6);
                j = (int) (Math.random()*6);
                imageView1.setImageResource(ArrayDraw[i]);
                imageView2.setImageResource(ArrayDraw[j]);
                rollButton.setText("Dice Rolled!");
                Toast.makeText(getApplicationContext(), "Fet!",  Toast.LENGTH_SHORT ).show();
                if (i == 5 && j == 5){
                    Toast toast = Toast.makeText(getApplicationContext(), "JACKPOT!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0,0);
                    toast.show();
                }
            }
        });

        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView1.setImageResource(R.drawable.empty_dice);
                imageView2.setImageResource(R.drawable.empty_dice);
            }
        });

        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = (int) (Math.random()*6);
                imageView1.setImageResource(ArrayDraw[i]);
                if (i == 5 && j == 5){
                    Toast toast = Toast.makeText(getApplicationContext(), "JACKPOT!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0,0);
                    toast.show();
                }
            }
        });

        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                j = (int) (Math.random()*6);
                imageView2.setImageResource(ArrayDraw[i]);
                if (i == 5 && j == 5){
                    Toast toast = Toast.makeText(getApplicationContext(), "JACKPOT!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0,0);
                    toast.show();
                }
            }
        });
    }
}